package ru.friday.calc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    public EditText pole1;
    public EditText pole2;
    public TextView pole4;
    public Button plus;
    public Button minus;
    public Button mylty;
    public Button del;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        pole1=(EditText)findViewById(R.id.editText);
        pole2=(EditText)findViewById(R.id.editText2);
        plus = (Button) findViewById(R.id.button2);
        minus =(Button) findViewById(R.id.button3);
        mylty = (Button) findViewById(R.id.button4);
        del = (Button) findViewById(R.id.button5);
        pole4=(TextView)findViewById(R.id.textView5);
    }
    public void OnClick(View view) {

            Intent intent = new Intent(MyActivity.this, TwoActivity.class);
            intent.putExtra("ID", view.getId());
            intent.putExtra("pole1", pole1.getText().toString());
            intent.putExtra("pole2", pole2.getText().toString());
            startActivity(intent);
    }



}
