package ru.friday.calc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class TwoActivity extends Activity{
    public TextView pole3;
    public EditText pole1;
    public EditText pole2;
    String chars = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two);
        pole3=(TextView)findViewById(R.id.textView3);
        pole1=(EditText)findViewById(R.id.editText);
        pole2=(EditText)findViewById(R.id.editText2);
        calc();
    }

    public void calc(){
        int id = getIntent().getExtras().getInt("ID");
        try {
        double res=0;
        double first= Double.parseDouble(getIntent().getStringExtra("pole1"));
        double second= Double.parseDouble(getIntent().getStringExtra("pole2"));
            switch (id) {
                case R.id.button2:
                    chars = "+";
                    res = first + second;
                    if(second > 0)
                    pole3.setText(getIntent().getStringExtra("pole1") + chars + getIntent().getStringExtra("pole2") + "=" + res);
                    else pole3.setText(getIntent().getStringExtra("pole1") + chars +"(" + getIntent().getStringExtra("pole2")+")" + "=" + res);
                    break;
                case R.id.button3:
                    chars = "-";
                    res = first - second;
                    if(second > 0)
                        pole3.setText(getIntent().getStringExtra("pole1") + chars + getIntent().getStringExtra("pole2") + "=" + res);
                    else pole3.setText(getIntent().getStringExtra("pole1") + chars +"(" + getIntent().getStringExtra("pole2")+")" + "=" + res);
                    break;
                case R.id.button4:
                    chars = "*";
                    res = first * second;
                    if(second > 0)
                        pole3.setText(getIntent().getStringExtra("pole1") + chars + getIntent().getStringExtra("pole2") + "=" + res);
                    else pole3.setText(getIntent().getStringExtra("pole1") + chars +"(" + getIntent().getStringExtra("pole2")+")" + "=" + res);
                    break;
                case R.id.button5:
                    if (second != 0) {
                        chars = "/";
                        res = first / second;
                        if(second > 0)
                            pole3.setText(getIntent().getStringExtra("pole1") + chars + getIntent().getStringExtra("pole2") + "=" + res);
                        else pole3.setText(getIntent().getStringExtra("pole1") + chars +"(" + getIntent().getStringExtra("pole2")+")" + "=" + res);
                    } else
                        pole3.setText("приличные люди на 0 не делят");
                    break;
            }
        } catch (NumberFormatException e) {
            pole3.setText("Я ниче не могу");
        }

    }

    public void OnClick(View view) {
        Activity activity = this;
        activity.finish();
    }
}
